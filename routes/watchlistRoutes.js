const express = require('express') // Import express
const router = express.Router() // Make router from app
const passport = require('passport'); // import passport
//const auth = require('../middlewares/auth/index'); // import passport auth strategy
const WatchlistController = require('../controllers/watchlistController.js') // Import TransaksiController
const watchlistValidator = require('../middlewares/validators/watchlistValidator.js') // Import validator to validate every request from user

router.get('/', [passport.authenticate('visitor', { session: false })], watchlistValidator.getAll, WatchlistController.getAll)
router.post('/:id/create', [passport.authenticate('visitor', { session: false })], watchlistValidator.create, WatchlistController.create)
router.delete('/delete/:id', [passport.authenticate('visitor', { session: false })], watchlistValidator.delete, WatchlistController.delete)

module.exports = router;
