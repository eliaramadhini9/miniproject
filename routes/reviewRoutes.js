const express = require('express') // Import express
const router = express.Router() // Make router from app
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth/index'); // import passport auth strategy
const ReviewController = require('../controllers/reviewController.js') // Import TransaksiController
const reviewValidator = require('../middlewares/validators/reviewValidator.js') // Import validator to validate every request from user

router.get('/', reviewValidator.getAll, ReviewController.getAll) // If accessing localhost:3000/transaksi, it will call getAll function in TransaksiController class
router.get('/:id',reviewValidator.getOne,ReviewController.getOneById) // If accessing localhost:3000/transaksi, it will call getAll function in TransaksiController class
router.get('/user/:user',reviewValidator.getByUser, ReviewController.getByUser) // If accessing localhost:3000/transaksi/:id, it will call getOne function in TransaksiController class
router.get('/movie/:movie',reviewValidator.getByMovie, ReviewController.getByMovie)
router.post('/:id/create',[passport.authenticate('visitor', { session: false })], reviewValidator.create, ReviewController.create) // If accessing localhost:3000/transaksi/create, it will call create function in TransaksiController class
router.put('/update/:id',[passport.authenticate('visitor', { session: false })], reviewValidator.update, ReviewController.update) // If accessing localhost:3000/transaksi/update/:id, it will call update function in TransaksiController class
router.delete('/delete/:id',[passport.authenticate(['visitor','admin'], { session: false })],reviewValidator.delete,  ReviewController.delete) // If accessing localhost:3000/transaksi/delete/:id, it will call delete function in TransaksiController class

module.exports = router; // Export router
