const express = require('express') // Import express
const app = express() // Make express app
const bodyParser = require('body-parser') // Import bodyParser
const movieRoutes = require('./routes/movieRoutes')
const userRoutes = require('./routes/userRoutes');
const reviewRoutes = require('./routes/reviewRoutes');
const watchlistRoutes = require('./routes/watchlistRoutes');


//Set body parser for HTTP post operation
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})); // support encoded bodies

//set static assets to public directory
app.use(express.static('public'));

app.use('/', userRoutes);
app.use('/movie', movieRoutes);
app.use('/review', reviewRoutes);
app.use('/watchlist', watchlistRoutes);


app.listen(3000, () => console.log('Server running on localhost:3000')) // Run server with port 3000

module.exports = app; // exports app
