const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const MovieSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  genre: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  runtime: {
    type: String,
    required: true
  },
  release: {
    type: String,
    required: true
  },
  rated: {
    type: String,
    required: true,
    upercase: true
  },
  siteRating:{
    type:Number,
    required:false,
    default: 0
  },
  cast: {
    type: String,
    required: true
  },
  imdb_rating: {
    type: Number,
    required: false
  },
  synopsis: {
    type: String,
    required: false
  },
  director: {
    type: String,
    required: true
  },
  writer: {
    type: String,
    required: true
  },
  trailer: {
    type: String,
    default: null,
    required: false
  },
  poster: {
    type: String,
    default: null,
    required: false
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false
})

MovieSchema.path('poster').get((img) => {
  if (img !== null ) {
    return '/poster/' + img
  } else {
    return '/poster/trailer.jpg'
  }
})

MovieSchema.path('trailer').get((str) => {
    return str
})

MovieSchema.set('toJSON', {
  getters: true
})

MovieSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
});

module.exports = movie = mongoose.model('movie', MovieSchema, 'movie');
