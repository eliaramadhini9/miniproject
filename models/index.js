const mongoose = require('mongoose');

const uri = "mongodb://localhost:27017/MiniProject"

mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false })

const review = require('./review.js')
const movie = require('./movie.js')
const user = require('./user.js')
const watchlist = require('./watchlist.js')

module.exports = { review, movie, user, watchlist};
