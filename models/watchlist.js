const mongoose = require("mongoose");
const mongoose_delete = require('mongoose-delete');

const WatchlistSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  movie: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false
});

WatchlistSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
});

module.exports = watchlist = mongoose.model("watchlist", WatchlistSchema, "watchlist");
