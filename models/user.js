const mongoose = require("mongoose"); // Import mongoose
const mongoose_delete = require('mongoose-delete'); // Import mongoose-delete to make soft delete
const bcrypt = require('bcrypt')

const UserSchema = new mongoose.Schema({

  fullname: {
    type: String,
    allowNull: false,
    unique: false
  },
  username: {
    type: String,
    unique: true,
    allowNull: false
  },

  email: {
    type: String,
    unique: true,
    allowNull: false
  },
  password: {
    type: String,

  },
  role: {
    type: String,
    required: true,
    default: 'visitor'

  },
  watchlist: {
    type: mongoose.Schema.Types.Mixed,
    allowNull: true,
    required: false

  },
  profilePic: {
    type: String,
    required: false,
    default: null
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false // Disable versioning __v = 0,

})

UserSchema.path('profilePic').get((img) => {
  if (img !== null) {
    return '/user/' + img
  } else {
    return '/user/profile-picture.jpg'
  }
})

UserSchema.set('toJSON', {
  getters: true
})

UserSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
}); // enable soft delete
UserSchema.methods.comparePassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};


module.exports = user = mongoose.model('user', UserSchema, 'user'); // export barang model
