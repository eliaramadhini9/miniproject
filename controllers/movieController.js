const {
  movie,
  review
} = require('../models')

class MovieController {

  async getAll(req, res) {
    const count = await movie.countDocuments();
    const {
      page = 1, limit = 10
    } = req.query;

    movie.find({}).limit(limit * 1).skip((page - 1) * limit).then(result => {
      res.json({
        status: "Success",
        data: result,
        totalPages: Math.ceil(count / limit),
        currentPage: page,
        totalData: count
      })
    })
  }

  async getOne(req, res) {
    movie.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async getByTitle(req, res) {
    function capitalizeFirstLetter(str) {
      return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()
    }
    let search = capitalizeFirstLetter(req.params.title)

    const {
      page = 1, limit = 10
    } = req.query;

    const data = await movie.find({
      title: {
        $regex: '.*' + search + '.*'
      }
    })
    const count = data.length

    movie.find({
      title: {
        $regex: '.*' + search + '.*'
      }
    }).limit(limit).skip((page - 1) * limit).then(result => {
      res.json({
        status: "Success",
        data: result,
        totalPages: Math.ceil(count / limit),
        currentPage: page,
        totalData: count
      })
    })
  }

  async getByGenre(req, res) {
    function capitalizeFirstLetter(str) {
      return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()
    }
    let search = capitalizeFirstLetter(req.params.genre)

    const {
      page = 1, limit = 10
    } = req.query;

    const data = await movie.find({
      genre: {
        $regex: '.*' + search + '.*'
      }
    })
    const count = data.length

    movie.find({
      genre: {
        $regex: '.*' + search + '.*'
      }
    }).limit(limit).skip((page - 1) * limit).then(result => {
      res.json({
        status: "Success",
        data: result,
        totalPages: Math.ceil(count / limit),
        currentPage: page,
        totalData: count
      })
    })
  }

  async create(req, res) {
    // function capitalizeFirstLetter(str) {
    //   var sentence = str.toLowerCase().split(" ")
    //     for (var i = 0; i < sentence.length; i++) {
    //       sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1)
    //   }
    //   return sentence.join(" ")
    // }
    movie.create({
      title: req.body.title,
      genre: req.body.genre,
      runtime: req.body.runtime + (' minutes'),
      release: req.body.release,
      rated: req.body.rated,
      cast: req.body.cast,
      imdb_rating: req.body.imdb_rating,
      synopsis: req.body.synopsis,
      director: req.body.director,
      writer: req.body.writer,
      poster: req.file === undefined ? "" : req.file.filename,
      trailer: req.body.trailer
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async update(req, res) {
    var sum = 0
    const titles = await movie.findOne({
      _id: req.params.id
    })
    const data = await review.find({
      movie: titles.title
    })

    for (var i = 0; i < data.length; i++) {
      sum += data[i].rating
      var ave = sum / data.length
    }

    let newData = {}

    if (req.body.title) newData.title = req.body.title
    if (req.body.genre) newData.genre = req.body.genre
    if (req.body.runtime) newData.runtime = req.body.runtime + " minutes"
    if (req.body.rated) newData.rated = req.body.rated
    if (req.body.cast) newData.cast = req.body.cast
    if (req.body.release) newData.release = req.body.release
    if (req.body.director) newData.director = req.body.director
    if (req.body.imdb_rating) newData.imdb_rating = req.body.imdb_rating
    if (req.body.synopsis) newData.synopsis = req.body.synopsis
    if (req.body.writer) newData.writer = req.body.writer
    if (req.body.trailer) newData.trailer = req.body.trailer
    if (req.file) newData.poster = req.file === undefined ? "" : req.file.filename

    movie.findOneAndUpdate({
      _id: req.params.id
    }, {
      siteRating: ave,
      $set: newData
    }, {
      new: true
    }).then(() => {
      return movie.findOne({
        _id: req.params.id
      })
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async delete(req, res) {
    movie.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: "Success",
        data: null
      })
    })
  }

}

module.exports = new MovieController
