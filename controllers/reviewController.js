const {
  review
} = require('../models');
const passportJWT = require("passport-jwt");
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const passport = require('passport')
const jwt = require('jsonwebtoken'); // import jsonwebtoken
const jwt_decode = require('jwt-decode');
//  const jwttoken=require ('../middlewares/auth/index.js')

class ReviewController {

  async getAll(req, res) {
    const count = await review.countDocuments();
    const {
      page = 1, limit = 10
    } = req.query;

    review.find({}).limit(limit * 1).skip((page - 1) * limit).then(result => {
      res.json({
        status: "Success",
        data: result,
        totalPages: Math.ceil(count / limit),
        currentPage: page,
        totalData: count
      })
    })
  }

  async getOneById(req, res) {
    review.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: "success",
        data: result
      })
    })
  }

  async getByMovie(req, res) {
    const {
      page = 1, limit = 10
    } = req.query;

    const data = await review.find({
      movie: {
        $regex: '.*' + req.params.movie + '.*'
      }
    })
    const count = data.length

    review.find({
      movie: {
        $regex: '.*' + req.params.movie + '.*'
      }
    }).limit(limit * 1).skip((page - 1) * limit).then(result => {
      res.json({
        status: "Success",
        data: result,
        totalPages: Math.ceil(count / limit),
        currentPage: page,
        totalData: count
      })
    })
  }

  async getByUser(req, res) {
    const {
      page = 1, limit = 10
    } = req.query;

    const data = await review.find({
      username: {
        $regex: '.*' + req.params.user + '.*'
      }
    })
    const count = data.length

    review.find({
      username: {
        $regex: '.*' + req.params.user + '.*'
      }
    }).limit(limit * 1).skip((page - 1) * limit).then(result => {
      res.json({
        status: "Success",
        data: result,
        totalPages: Math.ceil(count / limit),
        currentPage: page,
        totalData: count
      })
    })
  }

  async create(req, res) {
    const data = await Promise.all([
      movie.findOne({
        _id: req.params.id
      })
    ])
    review.create({
      movie: data[0].title,
      username: req.user.username,
      review: req.body.review,
      rating: req.body.rating
    }).then(result => {
      res.json({
        status: "success",
        data: result
      })
    })
  }

  async update(req, res) {
    let newData = {}

    if (req.body.review) newData.review = req.body.review
    if (req.body.rating) newData.rating = req.body.rating

    review.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: newData
    }, {
      new: true
    }).then(() => {
      return review.findOne({
        _id: req.params.id
      })
    }).then(result => {
      res.json({
        status: "success",
        data: result
      })
    })
  }

  async delete(req, res) {
    review.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: "success",
        data: null
      })
    })
  }

}

module.exports = new ReviewController;
