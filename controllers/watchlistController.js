const {
  watchlist,
  movie
} = require('../models');
const passportJWT = require("passport-jwt");
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const passport = require('passport')
const jwt = require('jsonwebtoken'); // import jsonwebtoken
const jwt_decode = require('jwt-decode');
//  const jwttoken=require ('../middlewares/auth/index.js')


class WatchlistController {

  async getAll(req, res) {
    const {
      page = 1, limit = 10
    } = req.query;

    const list = await watchlist.find({
      username: req.user.username
    })
    const count = list.length

    watchlist.find({
      username: req.user.username
    }).limit(limit * 1).skip((page - 1) * limit).then(result => {
      res.json({
        status: 'success',
        data: result,
        totalPages: Math.ceil(count / limit),
        currentPage: page,
        totalData: count
      })
    })
  }

  async create(req, res) {
    const movies = await Promise.all([
      movie.findOne({
        _id: req.params.id
      }, 'poster title release runtime rated siteRating cast')
    ])
    watchlist.create({
      movie: movies[0],
      username: req.user.username
    }).then(result => {
      res.json({
        status: "success",
        data: result
      })
    })
  }

  async delete(req, res) {
    watchlist.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: "success",
        data: null
      })
    })
  }

}

module.exports = new WatchlistController;
