const {
  movie
} = require('../../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

const multer = require('multer'); //multipar form-data
const path = require('path'); // to detect path of directory
const crypto = require('crypto'); // to encrypt something

const uploadDir = '/poster/'; // make images upload to /img/
const storage = multer.diskStorage({
  destination: "./public" + uploadDir, // make images upload to /public/img/
  filename: function(req, file, cb) {
    crypto.pseudoRandomBytes(16, function(err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
    })
  }
})

function isURL(str) {
  return str.match(/https:\/\/www.youtube.com\/watch\?v\=\w+/)
}

const upload = multer({
  storage: storage,
  dest: uploadDir
});

function capitalizeFirstLetter(str) {
  var sentence = str.toLowerCase().split(" ")
  for (var i = 0; i < sentence.length; i++) {
    sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1)
  }
  return sentence.join(" ")
}


module.exports = {
  getAll: [
    check('movie').custom(() => {
      return movie.find({}).then(result => {
        if (result.length == 0) {
          throw new Error('there are no movies')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  getOne: [
    check('id').custom(value => {
      return movie.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('movie\'s ID doesn\'t exist!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  getByTitle: [
    check('title').custom(value => {
      let search = capitalizeFirstLetter(value)
      return movie.find({
        title: {
          $regex: '.*' + search + '.*'
        }
      }).then(result => {
        if (result.length == 0) {
          throw new Error(`movie\'s title doesn\'t exist!`)
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  getByGenre: [
    check('genre').custom(value => {
      let search = capitalizeFirstLetter(value)
      return movie.find({
        genre: {
          $regex: '.*' + search + '.*'
        }
      }).then(result => {
        if (result.length == 0) {
          throw new Error(`genre doesn\'t exist!`)
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  create: [
    upload.single('poster'),
    check('title').isString().custom((value, {
      req
    }) => {
      let search = capitalizeFirstLetter(value)
      return movie.find({
        $and: [{
            title: search
          },
          {
            release: req.body.release
          },
          {
            director: req.body.director
          },
        ]
      }).then(result => {
        if (result.length !== 0) {
          throw new Error('movie already exist!')
        }
      })
    }),
    check('genre').isString().notEmpty(),
    check('runtime').isString().notEmpty(),
    check('release').isString().notEmpty(),
    check('rated').isString().notEmpty(),
    check('imdb_rating').isNumeric().notEmpty(),
    check('cast').isString().notEmpty(),
    check('director').isString(),
    check('writer').isString(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  update: [
    upload.single('poster'),
    check('id').custom(value => {
      return movie.findOne({
        _id: value
      }).then(b => {
        if (!b) {
          throw new Error('movie\'s ID not exist!');
        }
      })
    }),
    check('title').custom(value => {
      return movie.findOne({
        title: value
      }).then(result => {
        if (result) {
          throw new Error('movie\'s name already exists')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  delete: [
    check('id').custom(value => {
      return movie.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('movie\'s ID not exist!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ]
}
