const {
  watchlist,
  movie
} = require('../../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params


module.exports = {
  getAll: [
    check('watchlist').custom((value, {
      req
    }) => {
      return watchlist.find({
        username: req.user.username
      }).then(result => {
        if (result.length == 0) {
          throw new Error('there are no watchlist')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  create: [
    check('movie').custom(async (value, {
      req
    }) => {
      const movies = await Promise.all([
        movie.findOne({
          _id: req.params.id
        }, 'poster title release runtime rated siteRating cast')
      ])
      return watchlist.find({
        username: req.user.username,
        movie: movies[0]
      }).then(result => {
        console.log(result.length);
        if (result.length !== 0) {
          throw new Error('this movie already added to a watchlist')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  delete: [
    check('id').custom((value, {
      req
    }) => {
      return watchlist.findOne({
        _id: req.params.id
      }).then(b => {
        if (!b) {
          throw new Error('watchlist\'s ID not exist!')
        }
      })
    }),
    check('id').custom((value, {
      req
    }) => {
      return watchlist.findOne({
        _id: req.params.id
      }).then(b => {
        if (b.username !== req.user.username) {
          throw new Error('erorrrrrr');
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ]
};
